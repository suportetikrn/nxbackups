# README #
Script para ajudar o envio dos backups do NxFilter para servidores na internet.

### REQUISITOS ###
 * Ter instalado o [NxFilter](https://nxf.kernel.inf.br) - [Instalando o  NxFilter](http://docs.nxf.kernel.inf.br/pt_BR/latest/pages/getting_started/install.html)
 * Ter um servidor FTP instalado e acessível pela internet
 * Ter instalado o GIT
 * Entender a sintaxe do Crontab - [aqui](https://pt.wikipedia.org/wiki/Crontab)


### PARAMETERS / PARÂMETROS ###

 * NXFILTER - Diretório onde o nxfilter está instalado ( ex.: "/nxfilter")
 * DIR_BACKUP - Diretório onde os arquivos de backup serão arquivados, sem barra no final
 * ARQ_BACKUP - Nome do arquivo de backup
 * FTP_SERVER - Endereço do servidor FTP
 * FTP_USER - Usuário com acesso ao servidor FTP
 * FTP_PWD - Senha do Servidor FTP
 * FTP_DIR - Diretório no Servidor FTP onde ficarão os arquivos de backup

### Instalando ###
```bash
cd /
git clone https://gitlab.com/suportetikrn/nxbackups.git
cd /nxbackups
chmod +x *.sh
./install.sh
```
