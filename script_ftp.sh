#!/bin/bash

### AJUSTE OS PARAMETROS DE ACORDO COM SEU AMBIENTE

# diretorio a ser Salvo!
NX_ORIGEM="/nxfilter/"
# diretorio aonde sera feito o backup provisoriamente antes de enviar
DIR_BACKUP="/nxbackups/backups/temp/"
#Configuracao para data no arquivo de backup
ARQ_BACKUP="backup_nxfilter--`date +%d_%m_%Y__%H_%M`"


# Endereco do servidor de backup
FTP_SERVER="10.1.1.9"
# Usuario do servidor de backup
FTP_USER="bkp_ftp_nxlav"
# Senha de Acesso ao servidor de backup
FTP_PWD="33511515"
# Diretorio no servidor ftp onde ficara armazenado o backup
FTP_DIR="/"

### A PARTIR DAQUI NÃƒO ALTERE

echo "#####Entrando no diretorio de envio de arquivos"
cd $DIR_BACKUP


# fazendo o backup
echo "#####Fazendo Backup - Compactando 1  .tbz - Numero 1..."
tar -cjvf $ARQ_BACKUP.tbz $NX_ORIGEM


echo "Confirmando Diretorio..."
pwd

# espere por segundos
sleep 3

# conecte-se ao servidor FTP e envie o arquivo
echo "#####conectando no servidor FTP..."

ftp -ivn $FTP_SERVER << FTP
user $FTP_USER $FTP_PWD

echo "#####Conectado e dentro do diretorio raiz."
cd $FTP_DIR
bin
echo "Exibindo Diretorio atual..."
pwd

#Upando Backup
echo "#####Fazendo upload do arquivo..."
put $ARQ_BACKUP.tbz

bye
EOF
FTP

#Removendo o arquivo compactado para liberar espaï¿½o
echo '##### Removendo o arquivo Compactado TBZ temporario $ARQ_BACKUP...'
rm $ARQ_BACKUP.tbz
